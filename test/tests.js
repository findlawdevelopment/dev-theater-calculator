describe('Calculator:', function () {
  var calc, createCalc;

  before(function () {
    
    createCalc = function () {

      calc = new Calculator('#calculator');

    };

  });

  afterEach(function () {
      $('#val1').val(' ');
      $('#val2').val(' ');
      $('#add').prop('checked', false);
      $('#subtract').prop('checked', false);
  });

  it('adds two values', function () {

    createCalc();

    var total = calc.add(1, 2);

    total.should.be.exactly(3);

  });

  it('subtracts two values', function () {
    
    createCalc();

    var total = calc.subtract(2, 1);

    total.should.be.exactly(1);

  });

  it('scrapes the form and returns the data', function () {
    
    $('#val1').val(1);
    $('#val2').val(2);
    $('#add').prop('checked', true);

    createCalc();

    var data = calc.scrapeForm();

    var mockData = {
      "val1": 1,
      "val2": 2,
      "operation": 'add'
    };

    data.should.eql(mockData);

  });

  it('calculates based on user input', function () {
    
    $('#val1').val(1);
    $('#val2').val(2);
    $('#add').prop('checked', true);

    createCalc();

    calc.calculate();

    $('.results').text().should.eql(3);

  });

});