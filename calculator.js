+function ($) {

  /**
   * Calcualtor class
   *
   * @class calculator
   * @param selector {String} - The element selector
   * @example
   *     var calc = new Calcualtor('#calculator');
   */
  var Calculator = function  (selector) {

    if (!selector) {
      console.error('Please provide a selector!!');
      return false;
    }

    this.form = $(selector);

  };

  /**
   * Function that adds two values
   *
   * @method add
   * @param val1 {Number} - the first number to be added
   * @param val2 {Number} - the second number to be added
   * @return {Number} - the total of the two numbers
   */
  Calculator.prototype.add = function (val1, val2) {

    return val1 + val2;

  };

  /**
   * Function that subtracts two values
   *
   * @method subtract
   * @param val1 {Number} - the first number to be subtracted
   * @param val2 {Number} - the second number to be subtracted
   * @return {Number} - The difference of the two numbers
   */
  Calculator.prototype.subtract = function (val1, val2) {

    return val1 - val2;

  };

  Calculator.prototype.scrapeForm = function () {
    /**
     * Grabs data from the form to be used in the calculator
     *
     * @method scrapeForm
     * @returns {this}
     * @chainable
     */

    var calc = this,
        form = calc.form
    ;

    // reset the data
    var data = {};

    form.find('input').each(function (i, ele) {
      var $el = $(ele);

      switch (ele.type) {

        case 'checkbox':
          data[$el.attr('id')] = $el.prop('checked');
          break;

        case 'radio':
          data[ele.name] = $('[name="' + ele.name + '"]:checked').val();
          break;

        default:
          data[$el.attr('id')] = $el.val();
      }


    });

    return data;

  };

  /**
   * Interacts with the html form and performs the operation the user sepcified
   * 
   * @return {Number} - the result of the equation
   */
  Calculator.prototype.calculate = function() {
    
    var form = this.form;

    var data = this.scrapeForm();

    if (!data.operation) {
      console.error('no operation found');
      return false;
    }

    var result = this[data.operation](parseInt(data.val1, 10), parseInt(data.val2, 10));

    $('.results').text(result);

  };



  window.Calculator = Calculator;
 

}(jQuery);